Feature description
(Summarize the feature)


Use cases
(List the use cases of this feature)

-
-
-


Benefits
(List the benefits of having this feature)

-
-
-


Affected modules
(List potential affected modules)

-
-
-


Theoretical background
(Give a short theoretical background)


Links/References
(Provide some helpful references or previous implementations of the feature)

-
-
-


/label -enhancement
/cc @project-manager
/assign @project-manager
