project(rhyme_string_tests)
set(test_subject rhyme_string)

set(factories)
set(test_deps)

set(rhyme_src_dir ../..)
include(${CMAKE_CURRENT_SOURCE_DIR}/${rhyme_src_dir}/../cmake/test_helper.cmake)
