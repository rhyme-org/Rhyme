project(rhyme_nombre_tests)
set(test_subject rhyme_nombre)

set(factories)

set(test_deps rhyme_assertion)

set(rhyme_src_dir ../..)
include(${CMAKE_CURRENT_SOURCE_DIR}/${rhyme_src_dir}/../cmake/test_helper.cmake)
