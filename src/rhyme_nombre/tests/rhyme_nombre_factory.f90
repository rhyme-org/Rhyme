module rhyme_nombre_factory
   use rhyme_nombre_base_unit_factory
   use rhyme_nombre_base_unit_chain_factory
   use rhyme_nombre_derived_unit_factory
   use rhyme_nombre_dimension_factory
   use rhyme_nombre_unit_factory
end module rhyme_nombre_factory
