module rhyme_chemistry_assertion
   use rhyme_chemistry
   use rhyme_assertion

   implicit none

   interface operator(.toBe.)
   end interface operator(.toBe.)

contains
end module rhyme_chemistry_assertion
