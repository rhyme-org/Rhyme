# **Logging**
unicode_plotting = enable
projection_axis = z
pseudocolor_layer = 1
colormap = viridis


# Report
report_pseudocolor = rho
report_pseudocolor = temp
report_pseudocolor = ntr_frac_0
report_phase_diagram = rho-temp
report_phase_diagram = p-temp
report_histogram = |v|
report_histogram = e_tot
report_frequency = 100  # steps


# Sanity Check
sanity_check = enable
sanity_check_frequency = 10
sanity_check_rho = enable 0d0 1d1 "kg / m^3"
sanity_check_vx = enable -1d1 1d1 "m / s"
sanity_check_vy = enable -1d1 1d1 "m / s"
sanity_check_vz = enable -1d1 1d1 "m / s"
sanity_check_e_tot = enable -1d1 1d1 "kg / m^3 * m^2 / s^2"
sanity_check_temp = enable 100 1d8 "K"
sanity_check_ntr_frac_0 = enable 0 1
sanity_check_ntr_frac_1 = enable 0 1
sanity_check_ntr_frac_2 = enable 0 1
sanity_check_abs_v = enable 0 1d2 "m / s"
sanity_check_Mach = enable 0 1d1
sanity_check_total_mass = enable 0.9 1.1
sanity_check_total_energy = enable 0.9 1.1


# **Initial Condition**
ic_type = simple

ic_grid = 128 128
ic_box_lengths = 1 1 "kpc"
ic_nlevels = 3
ic_redshift = 1.23d0


# **Runtime Options**
max_nboxes = 1 10 100


# **Internal Units**
density_unit = "kg / m^3"
length_unit = "m"
time_unit = "s"


# **Boundary Condition**
left_bc = reflective
right_bc = inflow 1.23d0 2.34d0 3.45d0 4.56d0 5.67d0 6.78d0 7.89d0 8.90d0
bottom_bc = periodic
top_bc = reflective


# **Thermodynamics**
ideal_gas_type = diatomic


# **CFL**
courant_number = .2d0


# **Exact Riemann Solver**
# ExactRS: vacuum_pressure vacuum_density tolerance n_iteraton
# DeepRS: vacuum_pressure vacuum_density deep_rs_model_path
riemann_solver = ExactRS
vacuum_pressure = 1.23d0
vacuum_density = 2.34d0
tolerance = 1d-6
n_iteration = 100


# **Slope Limiter**
slope_limiter = minmod
slope_limiter_omega = -1d0


# **MUSCL-Hancock solver**
solver_type = cpu_intensive


# **Chemistry**
elements = H He
element_abundances = .75d0 .25d0  # Composition


# **Ionization Equilibrium**
uvb_equilibrium = enable
uvb_model = HM12
uvb_self_shielding = enable

collisional_ionization_equilibrium = enable

photoionization_equilibrium = disable

ie_convergence_rate = 1e-2
ie_max_niterations = 1000

species_cases = case_a case_b case_a  # HII HeII HeIII

equilibrium_table_size = 1024 1024
equilibrium_table_temp_range = 1d2 1d7 'K'
equilibrium_table_density_range =  1d-3 1d6 'm_H / cm^3'


# **Drawing**
# canvas = uniform .125d0 0d0 0d0 .1d0 1e4 1d-2 2d-3 3d-4
canvas = density_power_law 1.23d4 1.23d4 2.34d5 3.45d6 -2d0 4.56d7 .125d0 0d0 0d0 .1d0 1e4 1d-2 2d-3 3d-4
canvas_update_pressure = .true.

shape = cuboid 1 1 56 128 2.34d-1
shape_filling = uniform absolute 1d0 0d0 0d0 1d0 1d4 1d-2 1d-3 1d-4 2d0 3d0 3d0 4d0 1d5 1d-3 1d-4 1d-5

shape = sharp-cuboid 1 1 56 128 2.34d-1
shape_filling = uniform 1d0 0d0 0d0 1d0 1d4 1d-2 1d-3 1d-4

shape = sphere 3d0 4d0 2.34d0 2.34d-1 'unit'
shape_filling = uniform absolute 1d0 0d0 0d0 1d0 1d4 1d-2 1d-3 1d-4 2d0 3d0 3d0 4d0 1d4 1d-3 1d-4 1d-5

shape = prism 56 1 56 128 72 1
shape_filling = uniform 1d0 0d0 0d0 1d0 1d4 1d-2 1d-3 1d-4

shape = smoothed_slab_2d x 56 72 .2 .4
shape_filling = .125d0 0d0 0d0 .1d0 1d4 1d-2 1d-3 1d-4 1d0 0d0 0d0 1d0 1d5 1d-3 1d-4 1d-5

perturb = harmonic cartesian x 0.05 32 0d0 0d0 0d0 1d0
perturb = symmetric_decaying cartesian y 1d0 56 2 0d0 0d0 0d0 1d0
perturb = symmetric_decaying cartesian y 1d0 72 8 0d0 0d0 0d0 1d0
perturb = wgn box_muller 2345 rho 0d0 1d2 1d0 0d0 30


# **Stabilizer**
stabilizer = enable
stabilizer_weight = rho
stabilizer_weight_power = 2
stabilizer_initialize_center = disable 64 64 64  # px
stabilizer_tolerance = 16 # px
stabilizer_min_time_interval = 10 # time steps
stabilizer_start_at = -1 # time step


# **Chombo Output**
chombo_prefix = './prefix'
chombo_nickname = 'hydro-simulation'
chombo_output_every = 100
chombo_output_restart_backup_every = 17
chombo_output_rule = log 1d-4 1d0 5
chombo_output_rule = linear 1.5d0 10d0 20
chombo_output_final_time = 1.23d4
